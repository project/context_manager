<?php

namespace Drupal\context_manager\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Context Ruleset entities.
 */
interface ContextRulesetInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
